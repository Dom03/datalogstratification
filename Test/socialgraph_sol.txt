
:-type(publish(w1:string,w2:string)).

publish('www.mapage.com','www.wordpress.com').
publish('www.mapage.com','www.facebook.com').
publish('www.facebook.com','www.mapage.com').
publish('www.mapage.com','www.twitter.com').
publish('www.instagram.com','www.facebook.com').
publish('www.twitter.com','www.plus.google.com').

:-type(publish_extended(w1:string,w2:string,d:int)).

publish_extended('www.mapage.com','www.lemonde.com',3).
publish_extended('www.mapage.com','www.facebook.com',1).
publish_extended('www.mapage.com','www.instagram.com',1).
publish_extended('www.instagram.com','www.lemonde.com',1).
publish_extended('www.facebook.com','www.lemonde.com',1).
publish_extended('www.instagram.com','www.mapage.com',1).

% IDB
tcr(X, Y) :- publish(X, Y).
tcr(X, Y) :- publish(X, Z), tcr(Z, Y).
tcl(X, Y) :- publish(X, Y).
tcl(X, Y) :- tcl(X, Z), publish(Z, Y).
tclr(X, Y) :- publish(X, Y).
tclr(X, Y) :- tclr(X, Z), tclr(Z, Y).
v(X) :- publish(X,_).
v(Y) :- publish(_,Y).
ctc(X,Y) :- v(X), v(Y), not tcr(X,Y).
p(X, Y) :- publish(X, Y).
p(X, Y) :- publish(X, U), p(U, Y).
odd(X,Y)  :- publish(X,Y).
even(X,Y) :- odd(X,Z), publish(Z,Y).
odd(X,Y)  :- even(X,Z), publish(Z,Y).
oddcycle(X) :- odd(X, X).
evencycle(X) :- even(X,X).
nocycle(X) :- v(X), not oddcycle(X), not evencycle(X).
v_extendend(X) :- publish_extended(X,_,_).
v_extendend(Y) :- publish_extended(_,Y,_).
d(D) :- v_extendend(X),v_extendend(Y),publish_extended(X,Y,D).
path_extended(X, D) :- v_extendend(X), d(D), publish_extended('www.mapage.com', X, D).
path_extended(X, D) :- v_extendend(Y), d(D1), d(D2), path_extended(Y, D1), publish_extended(Y, X, D2), D = D1 + D2.
min_path(M) :- min(path_extended(_, D), D , M).
edgecount(T, C) :- v(T), count_distinct(publish(_,T), C).
maxedgecount(M) :- max(edgecount(_,C), C, M).
mostlinked(X) :- edgecount(X,M), maxedgecount(M).