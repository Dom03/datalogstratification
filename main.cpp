#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <algorithm>
using namespace std;

void tokenizeHead(string const &str, string const &delim, vector<string> &out){
    string token = str.substr(0, str.find(delim));
    out.push_back(token);
}

void tokenizeBody(string const &str, string const &delim, vector<string> &out){
    string token = str.substr(str.find(delim) + 2, str.length());
    out.push_back(token);
}

int main () {

    ifstream dataFile("datalog.txt");// opening file

    //check for error
    if (dataFile.fail()) {
        cerr << "Error Opening File" << endl;
        exit(1);
    }

    vector <string> idbLines;
    
    // *************************** STEP 1 : PREPROCESSING *******************************

    if (dataFile.is_open()) {
        string line;

        while (!dataFile.eof() && line != "% IDB") {
            getline(dataFile, line);
        }//we read the file until we reach the IDB relations

        while (!dataFile.eof()) { //we stock the lines in a vector
            getline(dataFile, line);
            idbLines.push_back(line);
        }
        
        dataFile.close();
    }
    else cout << "Unable to open file" << endl;

    vector <string> idbHeads;
    vector <string> idbBody;
    
    string delim = "(";
    for (int i = 0; i < idbLines.size(); i++) 
        tokenizeHead(idbLines[i], delim, idbHeads);
    
    delim = ":-";
    for (int i = 0; i < idbLines.size(); i++) 
        tokenizeBody(idbLines[i], delim, idbBody);


    for (int i = 0; i < idbHeads.size(); i++) {
        for (int j = 0; j < idbHeads.size(); j++) {
            if (idbHeads[i] == idbHeads[j] && i!=j) { //if there is recursivity somewhere, we merge the bodies of the same head
                idbBody[i] = idbBody[i] + idbBody[j];
                idbBody.erase(idbBody.begin() + j);
                idbHeads.erase(idbHeads.begin() + j);
            }
        }
    }

    for (int i = 0; i < idbHeads.size(); i++) {
        cout << "head : " << idbHeads[i] << " => body : " << idbBody[i] <<  endl;
    }

    //creating 2D array which is a grid (we use a grid instead of a graph)
    //if tabSommet[1][3] == 1 then edge between 1 and 3, if tabSommet[1][3] == 0 then no edge between 1 and 3, 
    //if -1 then negative edge
    int** tabSommet = new int*[idbHeads.size()];
    for (int i = 0; i < idbHeads.size(); ++i)
        tabSommet[i] = new int[idbHeads.size()];

    for (int i = 0; i < idbHeads.size(); i++) //initializing 2D array at 0
        for (int j = 0; j < idbHeads.size(); j++)
            tabSommet[i][j] = 0;
        
    string neg = "not ";
    for (int i = 0; i < idbBody.size(); i++) { //for each body
        for (int j = 0; j < idbHeads.size(); j++) { 
            if (idbBody[i].find(idbHeads[j]) != string::npos) { //we look for each heads if it is in the body
                if (idbBody[i].find(neg + idbHeads[j]) != string::npos) { //if negativity then -1
                    tabSommet[i][j] = -1;
                }
                else tabSommet[i][j] = 1;
            }
        }
    }
    
    // *************************** STEP 2 :  STRATIFICATION *******************************

    //for each predicate p do { stratum[p] := 1; }
    vector<int> stratum(idbHeads.size(), 1);

    bool change, exceeds = false;
    int temp = 0;
    do {
        change = false;
        for (int i = 0; i < stratum.size(); i++) {
            for (int j = 0; j < stratum.size(); j++) {
                temp = stratum[i];
                if (tabSommet[i][j] == -1)
                    stratum[i] =  max(stratum[i], 1+stratum[j]);  
                else if (tabSommet[i][j] == 1)
                    stratum[i] =  max(stratum[i], stratum[j]);

                if (temp != stratum[i])
                    change = true;

                if (stratum[i] > stratum.size())
                    exceeds = true;
            }
        }
    } while (change && !exceeds);


    // *************************** STEP 3 :  OUTPUT GENERATION *******************************

    ofstream soluceFile("stratification.txt");
    if (soluceFile.is_open()) {
        for (int i = 0; i < stratum.size(); i++)
            soluceFile << "Predicate " << idbHeads[i] <<" => Stratum " << stratum[i] << '\n';
        soluceFile.close();
    }
    else cout << "Unable to write file" << endl;


    for(int i = 0; i < idbHeads.size(); ++i) {
        delete [] tabSommet[i];
    }
    delete [] tabSommet;

    return 0;
}
